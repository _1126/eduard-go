/*
Package eduard is a small tool to keep record of your financial status and transactions.

- it handles both depositing and disbursing money (only €).

- it enables quering the total amount of money at any given point in time.

- it provides a cli for your computer and an android app.

- it saves its data in a sqlite3 database that gets synchronized between the computer and the android device.
*/
package main

import (
	"fmt"
	"bitbucket.com/_1126/eduard/database"
)

func main() {
	fmt.Println("Hello, this will be eduard.")
	fmt.Printf("Its database will be called %s.\n", database.Name())
	fmt.Printf("The database path is: %s.\n", database.Path())
	fmt.Printf("The schema of the database is as follows:\n%s.\n", database.Schema())
}
