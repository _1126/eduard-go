// Package database will take care of all things eduard needs to do with its sqlite3 database.
package database

import (
	"strings"
	"os"
)

var baseDir string = ".eduard"
var name string = "eduard.sqlite"

var schema string = "CREATE TABLE finances (uuid TEXT NOT NULL PRIMARY KEY, amount INT NOT NULL, timestamp TEXT NOT NULL, transacted BOOLEAN NOT NULL CHECK (transacted IN (0, 1)), refers-to TEXT)"

// Returns the database name
func Name() string {
	return name
}

// Returns the database path
func Path() string {
	s := []string{os.Getenv("HOME"), baseDir, name}
	return strings.Join(s, "/")
}

// Returns the database creation statement, aka the schema.
func Schema() string {
	return schema
}
